import java.util.*;

import static java.lang.System.lineSeparator;

public class ManagerTasks {
    public static void main(String[] args) {

        //Afficher le menu principal -- créer une Liste avec index -- demander l'affichage du MenuPrincipal
        ArrayList<String> listMenuPrincipal = new ArrayList<>();
        listMenuPrincipal.add("BIENVENUE DANS VOTRE LISTE DE TACHES" + lineSeparator() + "Choisissez une tâche ci-dessous :" + lineSeparator());
        listMenuPrincipal.add("1-Ajouter une tâche" + lineSeparator());
        listMenuPrincipal.add("2-Consulter et modifier la liste des tâches" + lineSeparator());
        listMenuPrincipal.add("3-Consulter la liste des tâches archivées");
        System.out.println(listMenuPrincipal);

        // Saisie de l'utilisateur pour le choix de Classe
        Scanner sc = new Scanner(System.in);
        int userSaisieIndexPrincipalMenu = sc.nextInt();

        // Condition choix utilisateur
        if ((userSaisieIndexPrincipalMenu != 1) && (userSaisieIndexPrincipalMenu != 2) && (userSaisieIndexPrincipalMenu != 3)) {
            System.out.println(listMenuPrincipal);
        } else {
            // Menu Saisir une tache
            if (userSaisieIndexPrincipalMenu == 1) {
                System.out.println("Veuillez entrer un tâche" + lineSeparator() + "Ou entrez 'Q' pour retourner au menu principal");
                Task task = new Task();
                String userTask = task.getUserEntryTask();
             if (userTask.equals("Q")) {
                    System.out.println(listMenuPrincipal);
                } else {
                    System.out.println(userTask);
                }
            } else {
                if (userSaisieIndexPrincipalMenu == 2) {
                    System.out.println("Liste de tâches" + lineSeparator() + "Saisir le numéro de la ligne à supprimer");
                    Task task = new Task();
                    List<Task> userTask = task.getListOfTask();
                    System.out.println(userTask);
                } else {
                    System.out.println("Voici la liste de tâches archivées");
                }
            }

        }
    }
}









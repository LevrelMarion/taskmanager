import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Scanner;

public class TaskTestClient {

@Test
void ShouldWriteATask(){
    Scanner sc = new Scanner(System.in);
    String userTask = sc.nextLine();
    Assertions.assertEquals("test", userTask);
}


}
